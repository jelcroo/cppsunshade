#include "testprogram.h"

void testprogram::runAllTests() {
    testCheckPosition();
    testSetMotorFunctions();
    testPhotoSensor();
    testRainSensor();
    testTemperatureSensor();
    testWindspeedSensor();
    testUvSensor();
    printReport();
}

void testprogram::resetSensorValues() {
    photosensor.setSensorValue(11);
    rainsensor.setSensorValue(0);
    temperaturesensor.setSensorValue(30);
    uvsensor.setSensorValue(20);
    windspeedsensor.setSensorValue(0);
}

void testprogram::printReport() {
    cout<<"\n<<RESULTS>>"<<endl;
    cout<<"PASSES: "<<PASSES<<endl;
    cout<<"FAILS:  "<<FAILS<<endl;
    if (FAILS == 0) {
        cout<<"Test sequence result OK."<<endl;
    } else {
        cout<<"Test sequence result NOK."<<endl;
    }
}

void testprogram::testCheckPosition() {
    cout<<"\n<<CHECK SUNSHADE POSITION TEST>>"<<endl;
    //set variables for extended
    sunshadeExtended = true;
    sunshadeRetracted = false;
    //check if functions returns extended
    if (checkPosExtended() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASSES++;
    } else if (checkPosRetracted() == true) {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    //set variables for retracted
    sunshadeExtended = false;
    sunshadeRetracted = true;
    if (checkPosRetracted() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASSES++;
    } else if (checkPosExtended() == true) {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
}

void testprogram::testSetMotorFunctions() {
    //set condition to sunshade is closed/retracted
    sunshadeRetracted = true;
    sunshadeExtended = false;
    //call motor function to open
    setMotorExtend();
    //check if motor is now open
    if (checkPosExtended()) {
        cout<<"<PASS>"<<endl;
        PASSES++;
    } else {
        cout<<"<FAIL>"<<endl;
        FAILS++;
    }
    //set condition to sunshade is open/extended
    sunshadeRetracted = false;
    sunshadeExtended = true;
    //call motor function to open
    setMotorRetract();
    //check if motor is now open
    if (checkPosRetracted()) {
        cout<<"<PASS>"<<endl;
        PASSES++;
    } else {
        cout<<"<FAIL>"<<endl;
        FAILS++;
    }
}

void testprogram::testPhotoSensor() {
    resetSensorValues();
    cout<<"\n<<PHOTO SENSOR TEST>>"<<endl;
    //open sunshade for test purpose
    setMotorExtend();
    //set photosensorvalue to that sunshade should NOT close
    cout<<"<<set photosensor value to  "<<testValuePhotoExtend<<"  <<sunshade should now stay open>>"<<endl;
    photosensor.setSensorValue(testValuePhotoExtend);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosExtended() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS1 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    cout<<"<<set photosensor value to  "<<testValuePhotoRetract<<"  <<sunshade should now close>>"<<endl;
    photosensor.setSensorValue(testValuePhotoRetract);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosRetracted() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS2 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    testResult();
};

void testprogram::testRainSensor() {
    resetSensorValues();
    cout<<"\n<<RAIN SENSOR TEST>>"<<endl;
    //open sunshade for test purpose
    setMotorExtend();
    //set sensorvalue to that sunshade should NOT close
    cout<<"<<set photosensor value to  "<<testValueRainExtend<<"  <<sunshade should now stay open>>"<<endl;
    rainsensor.setSensorValue(testValueRainExtend);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosExtended() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS1 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    cout<<"<<set rainsensor value to  "<<testValueRainRetract<<"  <<sunshade should now close>>"<<endl;
    rainsensor.setSensorValue(testValueRainRetract);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosRetracted() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS2 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    testResult();
};

void testprogram::testTemperatureSensor() {
    resetSensorValues();
    cout<<"\n<<TEMP SENSOR TEST>>"<<endl;
    //open sunshade for test purpose
    setMotorExtend();
    //set psensorvalue to that sunshade should NOT close
    cout<<"<<set temperature value to  "<<testValueTempExtend<<"  <<sunshade should now stay open>>"<<endl;
    temperaturesensor.setSensorValue(testValueTempExtend);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosExtended() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS1 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    cout<<"<<set temperaturesensor value to  "<<testValueTempRetract<<"  <<sunshade should now close>>"<<endl;
    temperaturesensor.setSensorValue(testValueTempRetract);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosRetracted() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS2 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    testResult();
};
void testprogram::testWindspeedSensor() {
    resetSensorValues();
    cout<<"\n<<WIND SENSOR TEST>>"<<endl;
    //open sunshade for test purpose
    setMotorExtend();
    //set sensorvalue to that sunshade should NOT close
    cout<<"<<set sensor value to  "<<testValueWindExtend<<"  <<sunshade should now stay open>>"<<endl;
    windspeedsensor.setSensorValue(testValueWindExtend);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosExtended() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS1 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    cout<<"<<set rainsensor value to  "<<testValueWindRetract<<"  <<sunshade should now close>>"<<endl;
    windspeedsensor.setSensorValue(testValueWindRetract);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosRetracted() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS2 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    testResult();
};
void testprogram::testUvSensor() {
    resetSensorValues();
    cout<<"\n<<UV SENSOR TEST>>"<<endl;
    //close sunshade for test purpose
    setMotorRetract();
    //set sorvalue to that sunshade should NOT close
    cout<<"<<set sensor value to  "<<testValueUvRetract<<"  <<sunshade should now stay closed>>"<<endl;
    uvsensor.setSensorValue(testValueUvRetract);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosRetracted() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS1 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    cout<<"<<set sensor value to  "<<testValueUvExtend<<"  <<sunshade should now open>>"<<endl;
    uvsensor.setSensorValue(testValueUvExtend);
    //call monitor sensor change to act on set sensor value
    monitorSensorChange();
    //check status
    if(checkPosExtended() == true) {
        cout<<"Result: <PASS>"<<endl;
        PASS2 = true;
        PASSES++;
    } else {
        cout<<"Restult: <FAIL>"<<endl;
        FAILS++;
    }
    testResult();
};

void testprogram::testResult() {
    if (PASS1 == true && PASS2 == true) {
        cout<<"<<<TEST PASSED>>>"<<endl;
    } else {
        cout<<"<<<TEST FAILED>>>"<<endl;
    }
    PASS1 = false;
    PASS2 = false;
}
