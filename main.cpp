#define private public

#include <iostream>
#include "SunshadeDriver.h"
#include "photoSensor.h"
#include "rainSensor.h"
#include "uvSensor.h"
#include "temperatureSensor.h"
#include "windSpeedSensor.h"
#include "motor.h"
#include "testprogram.h"

using namespace std;

int main()
{
    //test object
    testprogram testprogram;
    //sunshade driver object
    SunshadeDriver sunshadedriver;

    //main loop function to run program -- uncomment to run program
/*
    while(1) {
        sunshadedriver.monitorSensorChange();
    };
*/
    ///test sequence -- uncomment to run test
    testprogram.runAllTests();

    return 0;
}
