#ifndef PHOTOSENSOR_H
#define PHOTOSENSOR_H
#define private public

class photoSensor
{
private:
    //variables
    int sensorValue;
    // Constructors/Destructors
    photoSensor();
    virtual ~photoSensor();
    //methods
    int readSensorValue();
    void setSensorValue(int value);
};

#endif // PHOTOSENSOR_H
