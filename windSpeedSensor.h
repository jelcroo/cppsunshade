#ifndef WINDSPEEDSENSOR_H
#define WINDSPEEDSENSOR_H

class windSpeedSensor
{
private:
    //variables
    int sensorValue = 9;
    // Constructors/Destructors
    windSpeedSensor();
    virtual ~windSpeedSensor();
    //methods
    int readSensorValue();
    void setSensorValue(int value);
};

#endif // WINDSPEEDSENSOR_H
