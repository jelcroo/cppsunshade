#ifndef TEMPERATURESENSOR_H
#define TEMPERATURESENSOR_H

class temperatureSensor
{
private:
    //variables
    int sensorValue = 30;
    // Constructors/Destructors
    temperatureSensor();
    virtual ~temperatureSensor();
    //methods
    int readSensorValue();
    void setSensorValue(int value);
};

#endif // TEMPERATURESENSOR_H
