#include "SunshadeDriver.h"

// Constructors/Destructors
SunshadeDriver::SunshadeDriver(){}
SunshadeDriver::~SunshadeDriver(){}
//methods
void SunshadeDriver::setMotorExtend() {
    //open sunshade
    if (_motor.rotateExtend()) {
        sunshadeRetracted = false;
        sunshadeExtended = true;
        cout<<"sunshade is now open/extended"<<endl;
    }
};
void SunshadeDriver::setMotorRetract() {
    //check if sunshade is not already closed
    if (checkPosExtended() == true) {
        //close sunshade
        if (_motor.rotateRetract()) {
            sunshadeRetracted = true;
            sunshadeExtended = false;
            cout<<"sunshade is now closed"<<endl;
        }
    }
};
void SunshadeDriver::setRoomTemp(int value) {
    tempLevel = value;
};
void SunshadeDriver::monitorSensorChange() {
    //checks position of sunshade to make sure it only closes while it is open
    if (checkPosExtended() == true) {
        //monitor sensors to close sunshade
        /// rain
        if (rainsensor.readSensorValue() > rainLevel) {
            setMotorRetract();
        }
        if (photosensor.readSensorValue() < lightLevel) {
            setMotorRetract();
        }
        if (windspeedsensor.readSensorValue() > windLevel) {
            setMotorRetract();
        }
        if (temperaturesensor.readSensorValue() < tempLevel) {
            setMotorRetract();
        }
        else {
            return;
        }
    }//make sure that sunshade only opens when it is closed
    else if (checkPosRetracted() == true) {
        if (uvsensor.readSensorValue() > uvLevel) {
            setMotorExtend();
        } else {
            return;
        }
    }//sunshade is closed
};
bool SunshadeDriver::checkPosRetracted() {
    if( sunshadeRetracted == true && sunshadeExtended == false) {
        return true;
    } else {
        cout<<"ERROR sunshade not in open or closed position"<<endl;
        return false;
    }
}
bool SunshadeDriver::checkPosExtended(){
    if( sunshadeRetracted == false && sunshadeExtended == true) {
        return true;
    } else {
        cout<<"ERROR sunshade not in open or closed position"<<endl;

        return false;
    }
}
