#ifndef RAINSENSOR_H
#define RAINSENSOR_H

class rainSensor
{
private:
    //variables
    int sensorValue = 9;
    // Constructors/Destructors
    rainSensor();
    virtual ~rainSensor();
    //methods
    int readSensorValue();
    void setSensorValue(int value);
};

#endif // RAINSENSOR_H
