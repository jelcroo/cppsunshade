#ifndef UVSENSOR_H
#define UVSENSOR_H

class uvSensor
{
private:
    //variables
    int sensorValue = 11;
    // Constructors/Destructors
    uvSensor();
    virtual ~uvSensor();
    //methods
    int readSensorValue();
    void setSensorValue(int value);
};

#endif // UVSENSOR_H
