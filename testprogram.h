#ifndef TESTPROGRAM_H
#define TESTPROGRAM_H
#define private public

#include "photoSensor.h"
#include "rainSensor.h"
#include "uvSensor.h"
#include "temperatureSensor.h"
#include "windSpeedSensor.h"
#include "motor.h"
#include "SunshadeDriver.h"


#include <iostream>

using namespace std;

class testprogram:public SunshadeDriver
{
private:
    //variables
    bool PASS1, PASS2 = false; //to check passes
    int PASSES = 0; //count passes
    int FAILS = 0; //count fails
    //test variables
    int testValuePhotoExtend = 99;
    int testValuePhotoRetract = 9;
    int testValueRainExtend = 9;
    int testValueRainRetract = 99;
    int testValueWindExtend = 9;
    int testValueWindRetract = 99;
    int testValueTempExtend = 99;
    int testValueTempRetract = 9;
    int testValueUvExtend = 99;
    int testValueUvRetract = 9;
    //test functions
    void testCheckPosition();
    void testSetMotorFunctions();
    void testPhotoSensor();
    void testRainSensor();
    void testTemperatureSensor();
    void testWindspeedSensor();
    void testUvSensor();
    void resetSensorValues();
    void runAllTests();
    void testResult();
    void printReport();
};
#endif // TESTPROGRAM_H
