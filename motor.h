#ifndef MOTOR_H
#define MOTOR_H
#include <iostream>

using namespace std;

class motor
{
private:
    // Constructors/Destructors
    motor();
    virtual ~motor();
    //methods
    bool rotateExtend();
    bool rotateRetract();
};

#endif // MOTOR_H
