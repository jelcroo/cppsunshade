#ifndef SUNSHADEDRIVER_H
#define SUNSHADEDRIVER_H

#include "photoSensor.h"
#include "rainSensor.h"
#include "uvSensor.h"
#include "temperatureSensor.h"
#include "windSpeedSensor.h"
#include "motor.h"

#include <iostream>

using namespace std;

class SunshadeDriver
{
private:
    //variables
    //these 2 variables are set when sunshade is open/closed to check the position and can then also be used to avoid opening of the sunshade while clopsing and vice versa
    bool sunshadeExtended = false;
    bool sunshadeRetracted = true;
    //sensor thresholds
    int uvLevel = 10;
    int rainLevel = 10;
    int tempLevel = 20;
    int windLevel = 10;
    int lightLevel = 10;
    // sensor objects
    photoSensor photosensor;
    rainSensor rainsensor;
    uvSensor uvsensor;
    temperatureSensor temperaturesensor;
    windSpeedSensor windspeedsensor;
    // motor object
    motor _motor;
    // Constructors/Destructors
    SunshadeDriver();
    virtual ~SunshadeDriver();
    //methods
    void setMotorExtend();
    void setMotorRetract();
    void setRoomTemp(int value);
    void monitorSensorChange();
    bool checkPosRetracted();
    bool checkPosExtended();
};

#endif // SUNSHADEDRIVER_H
